#!/bin/bash
alias ll='ls -ltr'
alias git="docker run -ti --rm -v $(pwd):/git -v $HOME/.ssh:/root/.ssh alpine/git"
alias convoy='sudo system-docker exec -it --privileged convoy convoy'
sudo system-docker start busydash
