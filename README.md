** Install Disk **

sudo ros install --device /dev/sda --cloud-config https://bitbucket.org/htemat/rancher/raw/7c1c8aba7d945b986740d3943a602c3153b1529d/cloud-config.yml

**Rancher config**

* alias git="docker run -ti --rm -v $(pwd):/git -v $HOME/.ssh:/root/.ssh alpine/git"
* git clone https://htemat@bitbucket.org/htemat/rancher.git
* cd rancher
* git add . 
* git status
* git commit -m "test"

* git config  user.email "htamet@gmail.com"